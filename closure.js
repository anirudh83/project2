function counterFactory() {
    let counter = 10;
    let obj ={
        'increment': function(){
            counter += 1;
            return counter;
        },
        'decrement': function(){
            counter -= 1;
            return counter;
        }    
    }
    return obj; 
}

function limitFunctionCallCount(cb, n) {
    if( cb === undefined || n === undefined){
        return null ;
    }
    let count = 1;
    return function(){
        if(count <= n){
            cb();
            count += 1;
        }
        else{
            return null;
        }
    }
}

function cacheFunction(cb) {
    let cache_obj = {};
    if (cb === undefined){
        return function (){};
    }
    return  function(arg){
        if (arg === undefined){
            return null;
        }
        if (cache_obj[arg] === undefined){
            cache_obj[arg] = cb(arg);
            return cb(arg) ;
        }
        else{
            return cache_obj[arg];
        }      
    };
}


module.exports = { counterFactory, limitFunctionCallCount, cacheFunction};

