let closure = require('./closure.js');

let test1 = closure.counterFactory() ;
console.log(test1.increment());
console.log(test1.decrement());
let test2 = closure.counterFactory();
test2.increment();


let cb = ()=>(console.log("cb is invoked"));
var test = closure.limitFunctionCallCount(cb,2);
test();
test();
test();
test();
var test = closure.limitFunctionCallCount(cb);
console.log(test);

var test = closure.limitFunctionCallCount(2);
console.log(test);

var test = closure.limitFunctionCallCount();
console.log(test);


cb = function(arg){   
    return arg*arg;
}
var test = closure.cacheFunction(cb);
console.log(test(5));
var test = closure.cacheFunction();
console.log(test(5));
var test = closure.cacheFunction(cb);
console.log(test());
console.log(test(5));
console.log(test(6));
console.log(test(7));
console.log(test(5));
