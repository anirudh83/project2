function keys(obj) {
    let key_array = [];
    for (var key in obj) {
        key_array.push(key);
    }
    return key_array;
}

function values(obj) {
    let val_array = [];
    for (var key in obj) {
        val_array.push(obj[key]);
    }
    return  val_array;
}

function mapObject(obj, cb) {
    let new_obj = {};
    if( cb === undefined || obj === undefined){
        return null;
    }
    for (let key in obj){
        new_obj[key] = cb(key,obj[key]);
    }
  return new_obj;
}

function pairs(obj) {
    let array = [];
    for (let key in obj){
        array.push([key,obj[key]]);
    }
    return array;
}

function invert(obj) {
    let invert_obj = {};
    for(let key in obj){
        invert_obj[obj[key]] = key;
    }
    return invert_obj;
}

function defaults(obj, defaultProps) {
    let newObj = {};
    if (defaultProps === undefined || obj === undefined){
        return null;
    }
    for (let key in defaultProps ){
        if (obj[key] === undefined){
            newObj[key] = defaultProps[key];
        }
        else{
            newObj[key] = obj[key];
        }
    }
    return newObj;
}

module.exports = {keys, values, mapObject, pairs, invert, defaults };