const array = require('./array.js');
const items = [1, 2, 3, 4, 5, 5];

array.each(items, (a) => {
    console.log(a*a);
  });
array.each([], (a) => {
    console.log(a*a);
});
array.each( (a) => {
  console.log(a*a);
});
array.each(items);
array.map();

console.log(array.map(items, (a) => {
    return (a*a*a);
  }));
console.log(array.map([], (a) => {
    return (a*a*a);
  }));
console.log(array.map(items));
console.log(array.map());


console.log(array.reduce(items, (sum,num) => {
    return (sum+num);
  },0));
console.log(array.reduce([], (a) => {
    return (sum+num);
  }));
console.log(array.reduce(items));
console.log(array.reduce());

let cb = (x) =>(4 == x ? true : false);
console.log(array.find(items,cb));
console.log(array.find(items));
console.log(array.find([],cb));
console.log(array.find());

cb = (x) =>(0 === x%2 ? true : false);
console.log(array.filter(items,cb));
console.log(array.filter(items));
console.log(array.filter([],cb));
console.log(array.filter());


const nestedArray = [1, [2], [[3]], [[[4]]]];
let flatArray=[];
console.log(array.flatten(nestedArray,flatArray));
const nestedArray2 = [[[[4]]],5, [2], [[3]], [[[4]]]]; 
console.log(array.flatten(nestedArray2));
const nestedArray3 = [[[[]]],,[], [[]], [[[]]]]; 
console.log(array.flatten(nestedArray3));
console.log(array.flatten([]));
console.log(array.flatten());