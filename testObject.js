object=require('./object.js');
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

console.log(object.keys(testObject));
console.log(object.keys({}));
console.log(object.keys());

console.log(object.values(testObject));
console.log(object.values({}));
console.log(object.values());

let cb=(key,value) =>(value);

console.log(object.mapObject({},cb));
console.log(object.mapObject(cb));
console.log(object.mapObject());
console.log(object.mapObject(testObject,cb));


console.log(object.pairs(testObject));
console.log(object.pairs({}));
console.log(object.pairs());


console.log(object.invert(testObject));
console.log(object.invert({}));
console.log(object.invert());


const testObject2 = {  age:800 };
const defaultObject = { name: 'Wonder Woman ', age: 800, location: 'Themyscira' };
console.log(object.defaults(testObject2,defaultObject));
console.log(testObject2)
console.log(object.defaults(testObject2));
console.log(object.defaults(defaultObject));
console.log(object.defaults());