function each(elements = [], cb) {
    if( cb === undefined || elements.length == 0){
        return [];
    }
    for (let index = 0; index < elements.length; index++){
        cb(elements[index]);
    }
}

function map(elements = [], cb) {
    let new_array = [];
    if( cb === undefined || elements.length === 0){
        return [];
    }
    for (let index = 0; index < elements.length; index++){
        new_array.push(cb(elements[index]));
    }
   return new_array;
}

function reduce(elements = [], cb, sum = 0) {
    if( cb === undefined || elements.length === 0){
        return null;
    }
    for (let index = 0; index < elements.length; index++){
       sum = cb(sum,elements[index]);
    }
   return sum;
}

function find(elements = [], cb) {
    if( cb === undefined || elements.length === 0){
        return null;
    }
    for (let index = 0; index < elements.length; index++){
       if (cb(elements[index])){
           return elements[index];
       }
    }
    return undefined;
}

function filter(elements, cb) {
    let fillterted  = [];
    if( cb === undefined || elements === undefined){
        return null;
    }
    for( let index = 0; index < elements.length; index++){
        if(cb(elements[index])){
            fillterted.push(elements[index]);
        }
    }
    return fillterted;

}

function flatten(elements,flatArray = []) {
    if( elements === undefined || elements === null){
        return null;
    }
    for (let index = 0; index < elements.length; index++){
        if (Array.isArray(elements[index])){
            flatten(elements[index],flatArray);
        }
        else{
            flatArray.push(elements[index]);
        }
    }
    return flatArray;
}

module.exports = { each, map, reduce, find, filter, flatten };
  